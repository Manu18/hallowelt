#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A5
char *substring(char *text, int beginIndex) {
    char *pointer;
    int length = strlen(text) + 1;
    int size = strlen(text) - beginIndex;
    pointer = malloc(size + 1);
    if (pointer == NULL) {
        printf("Unable to allocate memory.\n");
        exit(1);
    }
    int c;
    int i = 0;
    for (c = beginIndex; c < length ; c++) {
        *(pointer + i) = *(text + beginIndex);
        text++;
        i++;
    }
    *(pointer + i) = '\0';
    return pointer;
}

char *substringFromTo(char *text, int beginIndex, int endIndex) {
    char *pointer;
    int length = endIndex;
    int size = endIndex - beginIndex;
    pointer = malloc(size + 1);
    if (pointer == NULL) {
        printf("Unable to allocate memory.\n");
        exit(1);
    }
    int c;
    int i = 0;
    for (c = beginIndex; c < length ; c++) {
        *(pointer + i) = *(text + beginIndex);
        text++;
        i++;
    }
    *(pointer + i) = '\0';
    return pointer;
}

// A3
char* concat(char* str1, char* str2) {
    strcat(str1,str2);
    return str1;
}

int main() {
    char *test = "Hamburger";
    printf(substring(test,1));
    printf("\n");
    printf(substringFromTo(test,4,8));


    return 0;
}