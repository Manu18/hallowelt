#include <stdio.h>
#include <stdlib.h>

// A1
int* newIntArray(int size) {
    int *array = malloc(size * sizeof(int));
    return array;
}

// A2
int* dynArrayResize(int *arr, int size) {
    int *a = realloc(arr, size * sizeof(int));
    //free(arr);
    return a;
}

// A4
struct BigInts {
    int *arr;
    int size;
};
typedef struct BigInts BigInt;

void increase(BigInt *b, int next) {
    int s = ++b->size;
    b->arr = realloc(b->arr, s * sizeof(int));
    b->arr[s-1] = next;
}

void decrease(BigInt *b) {
    b->size--;
    b->arr[b->size] = 0;
    b->arr = realloc(b->arr, b->size * sizeof(int));
}

void printBigInt(BigInt *b) {
    printf("\nBigInt with size of: %d\n", b->size);
    for (int i = 0; i < b->size; i++) {
        printf("%d ", b->arr[i]);
    }
}

int main() {
    int *a = newIntArray(2);
    a[0] = 1;
    a[1] = 2;
    printf("%d %d \n", a[0], a[1]);
    int *b = dynArrayResize(a,1);
    printf("%d %d \n", b[0], b[1]);
    free(b);

    BigInt bigI;
    // Initial
    bigI.size = 1;
    bigI.arr = malloc(1* sizeof(int));
    bigI.arr[0] = 0;


    increase(&bigI, 1);
    increase(&bigI, 2);
    increase(&bigI, 3);
    increase(&bigI, 4);
    printBigInt(&bigI);
    decrease(&bigI);
    decrease(&bigI);
    decrease(&bigI);
    printBigInt(&bigI);


    return 0;
}