#include <stdio.h>
#include <math.h>

// A1
void wb() {
    char a;
    int b;
    float c;
    double d;
    printf("Speichergroessen: \n");
    printf("char: %d Byte \n", sizeof(a));
    printf("int: %d Byte \n", sizeof(b));
    printf("float: %d Byte \n", sizeof(c));
    printf("double: %d Byte \n", sizeof(d));

    // b
    int a1 = 125;
    int b1 = -39;

    printf("Zeichen: %c und %c\n", a1, b1);
    printf("vorzeichenlose Ganzzahl: %u und %u\n", a1, b1);
    printf("Hexadezimalzahl: %x und %x\n", a1, b1);
    printf("vorzeichenbehaftete Ganzzahl: %d und %d\n", a1, b1);
    printf("Fliesskommazahl mit einfacher Genauigkeit und 2 Nachkommastellen: %.2f und %.2f\n", a1, b1);
    printf("Fliesskommazahl mit doppelter Genauigkeit und in exponentieller Darstellung: %E und %E\n", a1, b1);
}

// A2
void op() {
    float f;
    do
    {
        printf("Geben sie eine positive Kommazahl ein\n");
        scanf("%f", &f);
    } while (f < 0);
    int fi = f;

    printf("Quadrat: %f\n",f*f);
    int mod = fi % 3;
    printf("mod 3: %d\n",mod);
    double sq = sqrt(f);
    printf("Wurzel: %lf\n",sq);
    double funct = (5 * f + 3) / (7 * (f - 1.5));
    printf("Funktion: %lf\n",funct);
    int bit1 = fi << 2;
    printf("Bitverschiebung1: %x\n",bit1);
    int bit2 = fi & 0x00F0;
    printf("Bitverschiebung2: %x\n",bit2);
    printf("Inkrementieren: %d\n",++fi);
}

// A3
double quadF() {
    double a;
    double b;
    double c;
    double erg1;
    double erg2;
    printf("Geben Sie die Zahlen a,b,c ein fuer die Gleichung: a.x2 + b.x + c = 0\n");
    scanf("%lf", &a);
    scanf("%lf", &b);
    scanf("%lf", &c);
    erg1 = ( -b + sqrt((b*b) - 4*a*c) )/ (2*a);
    erg2 = ( -b - sqrt((b*b) - 4*a*c) )/ (2*a);
    printf("Das Ergebnis ist: \nx1 = %.3lf  \nx2 = %.3lf \n", erg1, erg2);
}


int main() {
    //wb();
    //op();
    quadF();
    return 0;
}