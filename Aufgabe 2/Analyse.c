#include <stdio.h>
#include <math.h>

int compare(float a, float b) {
    float e = 0.005;
    float f = fabs(a-b);
    return (f < e);
}

int main() {
    float a = 0.1f+0.1f+0.1f+0.1f+0.1f+0.1f+0.1f+0.1f+0.1f+0.1f;
    // 1 nicht exakt 1
    float b = 10 * 0.1f;
    // 1 aber nicht exakt, daher else !=
    if (compare(a, b)) {
        printf("%.6f == %.6f\n", a, b);
    } else {
        printf("%.6f != %.6f\n", a, b);
    }

    return 0;
}

// Vergleich von Gleitkommazahlen solllte nicht über == durchgeführt werden. 0.1f lässt sich nicht in der IEE754 Form korrekt darstellen,
// sondern nur annäherungsweise