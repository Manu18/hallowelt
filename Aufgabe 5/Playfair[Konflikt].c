#include "ctype.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"


int contains(char c, char text[]) {
    for (int i = 0; i < strlen(text); i++) {
        if (text[i] == c) {
            return 1;
        }
    }
    return 0;
}

int findPos(char b, char (*matrix)[5]) {
    // Erstelle Zeile aus Matrix
    char zeile1[5 + 1];
    for (int l = 0; l < 5; l++) {
        zeile1[l] = tolower(matrix[0][l]);
    }
    zeile1[5] = '\0';

    char zeile2[5 + 1];
    for (int l = 0; l < 5; l++) {
        zeile2[l] = tolower(matrix[1][l]);
    }
    zeile2[5] = '\0';

    char zeile3[5 + 1];
    for (int l = 0; l < 5; l++) {
        zeile3[l] = tolower(matrix[2][l]);
    }
    zeile3[5] = '\0';

    char zeile4[5 + 1];
    for (int l = 0; l < 5; l++) {
        zeile4[l] = tolower(matrix[3][l]);
    }
    zeile4[5] = '\0';

    char zeile5[5 + 1];
    for (int l = 0; l < 5; l++) {
        zeile5[l] = tolower(matrix[4][l]);
    }
    zeile5[5] = '\0';

    // Erstelle Spalte aus Matrix
    char spalte1[5 + 1];
    for (int k = 0; k < 5; k++) {
        spalte1[k] = tolower(matrix[k][0]);
    }
    spalte1[5] = '\0';

    char spalte2[5 + 1];
    for (int k = 0; k < 5; k++) {
        spalte2[k] = tolower(matrix[k][1]);
    }
    spalte2[5] = '\0';

    char spalte3[5 + 1];
    for (int k = 0; k < 5; k++) {
        spalte3[k] = tolower(matrix[k][2]);
    }
    spalte3[5] = '\0';

    char spalte4[5 + 1];
    for (int k = 0; k < 5; k++) {
        spalte4[k] = tolower(matrix[k][3]);
    }
    spalte4[5] = '\0';

    char spalte5[5 + 1];
    for (int k = 0; k < 5; k++) {
        spalte5[k] = tolower(matrix[k][4]);
    }
    spalte5[5] = '\0';

    if (contains(b, zeile1)) {
        if (contains(b, spalte1)) {
            return 11;
        }
        if (contains(b, spalte2)) {
            return 12;
        }
        if (contains(b, spalte3)) {
            return 13;
        }
        if (contains(b, spalte4)) {
            return 14;
        }
        if (contains(b, spalte5)) {
            return 15;
        }
    }
    if (contains(b, zeile2)) {

        if (contains(b, spalte1)) {
            return 21;
        }
        if (contains(b, spalte2)) {
            return 22;
        }
        if (contains(b, spalte3)) {
            return 23;
        }
        if (contains(b, spalte4)) {
            return 24;
        }
        if (contains(b, spalte5)) {
            return 25;
        }

    }

    if (contains(b, zeile3)) {

        if (contains(b, spalte1)) {
            return 31;
        }
        if (contains(b, spalte2)) {
            return 32;
        }
        if (contains(b, spalte3)) {
            return 33;
        }
        if (contains(b, spalte4)) {
            return 34;
        }
        if (contains(b, spalte5)) {
            return 35;
        }
    }
    if (contains(b, zeile4)) {
        if (contains(b, spalte1)) {
            return 41;
        }
        if (contains(b, spalte2)) {
            return 42;
        }
        if (contains(b, spalte3)) {
            return 43;
        }
        if (contains(b, spalte4)) {
            return 44;
        }
        if (contains(b, spalte5)) {
            return 45;
        }

    }
    if (contains(b, zeile5)) {
        if (contains(b, spalte1)) {
            return 51;
        }
        if (contains(b, spalte2)) {
            return 52;
        }
        if (contains(b, spalte3)) {
            return 53;
        }
        if (contains(b, spalte4)) {
            return 54;
        }
        if (contains(b, spalte5)) {
            return 55;
        }
    }
}

char (*createMatrix(char* key0))[5] {
    char* ptr = key0;
    int keylength = strlen(key0);
    char key[strlen(key0) + 1];
    for (int i = 0; i < keylength; i++) {
        key[i] = *ptr++;
    }
    key[keylength] = '\0';

    int p1 = 0;
    while(p1 < keylength) {
        key[p1] = toupper(key[p1]);
        p1++;
    }
    char abc[] = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
    char g[] = "_________________________";

    static char matrix[5][5];

    int pos = 0;
    int pos2 = 0;
    int pos3 = 0;
    int booleanK = 0;
    for (int i = 0; i < 5; i++) { // Zeilen
        for (int j = 0; j < 5; j++) { // Spalten

            while (pos2 < strlen(abc) && booleanK == 1) {
                if (contains(abc[pos2],g)) {
                    pos2++;
                } else {
                    matrix[i][j] = abc[pos2];
                    g[pos3++] = abc[pos2];
                    if (pos2 == strlen(abc) - 1) {
                    }
                    pos2++;
                    break;
                }
            }

            while (pos < keylength) { // key befüllen
                if (contains(key[pos],g)) {
                    pos++;
                } else {
                    matrix[i][j] = key[pos];
                    g[pos3++] = key[pos];
                    if (pos >= keylength - 2) {
                        booleanK = 1;
                    }
                    pos++;
                    break;
                }
            }
        }
    }
    return matrix;
}


char* klartext(char* text0) {
    char* ptr = text0;
    char text[strlen(text0) + 1];
    for (int i = 0; i < strlen(text0); i++) {
        text[i] = *ptr++;
    }
    text[strlen(text0)] = '\0';
    char* kt = malloc(512*sizeof(char)); // 512 zeichen max
    //char kt[512];
    for (int i = 0; i < strlen(text); i++) {
        if (text[i] == 'j') {
            text[i] = 'i';
        }

        if (text[i] == ' ' || text[i] == ',' || text[i] == ';' || text[i] == '.') {
            for (int j = i; j < strlen(text); j++) {
                text[j] = text[j+1];
            }
        }
    }
    int pos = 0;
    int poskt = 0;
    while (pos < strlen(text) - 1) {
        if (text[pos] != text[pos+1]) {
            kt[poskt] = text[pos];
            kt[poskt + 1] = text[++pos];
            kt[poskt + 2] = ' ';
            poskt += 3;
            pos++;
        } else {
            kt[poskt] = text[pos];
            kt[poskt + 1] = 'x';
            kt[poskt + 2] = ' ';
            poskt += 3;
            pos++;
        }
    }
    if (pos != strlen(text)) {
        kt[poskt] = text[pos];
        kt[poskt + 1] = 'x';
        poskt += 2;
    }
    kt[poskt] = '\0';
    return kt;
}

void outputMatrix(int spalten, int zeilen, char (*arr)[spalten]) {
    for(int i = 0; i < zeilen; i++) {
        printf("\n");
        for (int j = 0; j < spalten; j++) {
            printf("%c\t", arr[i][j]);
        }
    }
}

char* codierung(char text[], char (*matrix)[5]) {
    int textpos = 0;
    int boolSpalten = 1;
    printf("\nCodierung: \n");

    while (textpos < strlen(text)) {
        for (int i = 0; i < 5; i++) {

            // Zeilen Ueberpruefung
            char zeile[5 + 1];
            for (int l = 0; l < 5; l++) {
                zeile[l] = tolower(matrix[i][l]);
            }
            zeile[5] = '\0';
            //printf("Zeile: %s\n", zeile);

            if (contains(text[textpos], zeile) && contains(text[textpos + 1], zeile)) {
                char *string = zeile;
                char *e;
                e = strchr(string, text[textpos]);
                int index = (int) (e - string);
                text[textpos] = zeile[(index + 1) % 5];

                e = strchr(string, text[textpos + 1]);
                index = (int) (e - string);
                text[textpos + 1] = zeile[(index + 1) % 5];
                textpos += 3;
                break;
            }

            // Spalten Ueberpruefung
            if (boolSpalten) {
                // Spalten
                for (int j = 0; j < 5; j++) {
                    char spalte[5 + 1];
                    for (int k = 0; k < 5; k++) {
                        spalte[k] = tolower(matrix[k][j]);
                    }
                    spalte[5] = '\0';
                    //printf("Spalte: %s \n", spalte);

                    if (contains(text[textpos], spalte) && contains(text[textpos + 1], spalte)) {
                        char *string = spalte;
                        char *e;
                        e = strchr(string, text[textpos]);
                        int index = (int) (e - string);
                        text[textpos] = spalte[(index + 1) % 5];

                        e = strchr(string, text[textpos + 1]);
                        index = (int) (e - string);
                        text[textpos + 1] = spalte[(index + 1) % 5];
                        textpos += 3;
                        break;
                    }
                }
                boolSpalten = 0;
            }
        }

        // Sontige Ueberpruefung
        char b = text[textpos];
        char b2 = text[textpos + 1];
        int charAtPos1 = findPos(b, matrix);
        int charAtPos2 = findPos(b2, matrix);
        char e1 = tolower(matrix[(charAtPos1 / 10) - 1][(charAtPos2 % 10) - 1]);
        char e2 = tolower(matrix[(charAtPos2 / 10) - 1][(charAtPos1 % 10) - 1]);
        text[textpos] = e1;
        text[textpos + 1] = e2;

        textpos += 3;
        boolSpalten = 1;
        printf("%s\n", text);
    }
    return text;
}

char* decodierung(char text[], char (*matrix)[5]) {
    int textpos = 0;
    int boolSpalten = 1;
    printf("\nDecodierung: \n");

    while (textpos < strlen(text)) {
        for (int i = 0; i < 5; i++) {

            // Zeilen Ueberpruefung
            char zeile[5 + 1];
            for (int l = 0; l < 5; l++) {
                zeile[l] = tolower(matrix[i][l]);
            }
            zeile[5] = '\0';
            //printf("Zeile: %s\n", zeile);

            if (contains(text[textpos], zeile) && contains(text[textpos + 1], zeile)) {
                char *string = zeile;
                char *e;
                e = strchr(string, text[textpos]);
                int index = (int) (e - string);
                text[textpos] = zeile[(index - 1) % 5];

                e = strchr(string, text[textpos + 1]);
                index = (int) (e - string);
                text[textpos + 1] = zeile[(index - 1) % 5];
                textpos += 3;
                break;
            }

            // Spalten Ueberpruefung
            if (boolSpalten) {
                // Spalten
                for (int j = 0; j < 5; j++) {
                    char spalte[5 + 1];
                    for (int k = 0; k < 5; k++) {
                        spalte[k] = tolower(matrix[k][j]);
                    }
                    spalte[5] = '\0';
                    //printf("Spalte: %s \n", spalte);

                    if (contains(text[textpos], spalte) && contains(text[textpos + 1], spalte)) {
                        char *string = spalte;
                        char *e;
                        e = strchr(string, text[textpos]);
                        int index = (int) (e - string);
                        text[textpos] = spalte[(index - 1) % 5];

                        e = strchr(string, text[textpos + 1]);
                        index = (int) (e - string);
                        text[textpos + 1] = spalte[(index - 1) % 5];
                        textpos += 3;
                        break;
                    }
                }
                boolSpalten = 0;
            }
        }

        // Sontige Ueberpruefung
        char b = text[textpos];
        char b2 = text[textpos + 1];
        int charAtPos1 = findPos(b, matrix);
        int charAtPos2 = findPos(b2, matrix);
        char e1 = tolower(matrix[(charAtPos2 / 10) - 1][(charAtPos1 % 10) - 1]);
        char e2 = tolower(matrix[(charAtPos1 / 10) - 1][(charAtPos2 % 10) - 1]);
        text[textpos] = e2;
        text[textpos + 1] = e1;

        textpos += 3;
        boolSpalten = 1;
        printf("%s\n", text);
    }
    return text;
}


int main() {
    char mode[7];
    char key[128];
    char text[512];

    printf("Moduls eingeben: (encode / decode)\n");
    //fgets(mode, 7, stdin);
    scanf("%s", mode);

    printf("Geben Sie den Schluessel ein:\n");
    //fgets(key, 128, stdin);
    scanf("%s", key);
    char** encM = createMatrix(key);

    if(strcmp("encode", mode) == 0) {
        // Verschluesseln
        printf("Geben sie den zu verschluesselnden Text ein (ohne leerzeichen):");
        //fgets(text, 512, stdin);
        scanf("%s", text);
        char* textT = klartext(text);
        printf("Zu verschluesseln: %s\n", textT);
        printf("Verschluesselungsmatrix:");
        outputMatrix(5,5,encM);
        char* code = codierung(textT,encM);
        printf("Verschluesselt: %s\n", code);
    } else {
        // Entschluesseln
        printf("Geben sie den verschluesselten Text ein (Format: xx xx xx ...):");
        //fgets(text, 512, stdin);
        scanf("%s", text);
        printf("Zu entschluesseln: %s\n", text);
        printf("Verschluesselungsmatrix:");
        outputMatrix(5,5,encM);
        char* entcode = decodierung(text,encM);
        printf("Entschluesselt: %s\n", entcode);
    }
    return 0;
}