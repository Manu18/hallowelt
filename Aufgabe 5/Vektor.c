#include "stdio.h"
#include "math.h"

struct Punkt {
    int x;
    int y;
    int z;
};
typedef struct Punkt Vektor;

// vektor bestimmen
// betrag ausrechnen
double vektorBerechnen(Vektor v1, Vektor v2) {
    Vektor v3;
    v3.x = v2.x - v1.x;
    v3.y = v2.y - v1.y;
    v3.z = v2.z - v1.z;

    double wurzel = (v3.x * v3.x) + (v3.y * v3.y) + (v3.z * v3.z);
    double betrag = sqrt(wurzel);
    return betrag;
}


int main() {

    Vektor p1;
    p1.x = 2;
    p1.y = 2;
    p1.z = 2;

    Vektor p2;
    p2.x = 5;
    p2.y = 5;
    p2.z = 5;

    double betrag = vektorBerechnen(p1,p2);
    printf("Betrag: %lf", betrag);
    return 0;
}