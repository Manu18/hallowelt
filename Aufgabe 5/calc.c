#include "stdio.h"
#include "stdlib.h"

//./calc op1 operation op2
// int or float
// +, -, * oder /
//$./calc 1 + 3
//1 + 3 = 4






int main(int argc, char* argv[]) {
    //printf("%d\n", argc);
    if (argc == 4) {
        double z1 = atof(argv[1]);
        double z2 = atof(argv[3]);
        double erg = 0.0;
        char op = argv[2][0];

        if (op == '+') {
            erg = z1 + z2;
        }
        else if (op == '-') {
            erg = z1 - z2;
        }else if (op == 'x') {
            erg = z1 * z2;
        }else if (op == '/') {
            erg = z1 / z2;
        }
        printf("%s %s %s = %.3lf",argv[1], argv[2], argv[3], erg);
    } else {
        printf("Es ist ein Fehler aufgetreten, falsche Parameter übergeben");
    }

    return 0;
}