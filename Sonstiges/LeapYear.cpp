#include <iostream>

int main() {
  int year;
  std::cout << "Input year: \n";
  std::cin >> year;

  if (year > 999 && year < 10000) {
    if (year % 4 == 0) {
      if (year % 100 == 0 && year % 400 != 0) {
         std::cout << "Year: " << year << " is not a leap year\n";
      } else if (year % 400 == 0) {
        std::cout << "Year: " << year << " is not a leap year\n";
      } else {
        std::cout << "Year: " << year << " is a leap year\n";
      }
    } else {
      std::cout << "Year: " << year << " is not a leap year\n";
    }
  }

}