#include <stdio.h>

// A2
int min(int a, int b, int c) {
    int min = a;
    if (a > b) {
        min = b;
    }
    if (min > c) {
        min = c;
    }
    return min;
}
int max(int a, int b, int c) {
    int max = a;
    if (a < b) {
        max = b;
    }
    if (max < c) {
        max = c;
    }
    return max;
}

int main() {
    int a = 5;
    int b = 13;
    int c = 7;

    printf("Minimum: %d \n", min(a,b,c));
    printf("Maximum: %d \n", max(a,b,c));

    return 0;
}