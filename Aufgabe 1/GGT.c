#include <stdio.h>

int ggT(int a, int b) {
    if(a == 0) {
        return b;
    } else {
        while (b != 0)  {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
}

int main() {
    int a;
    int b;
    scanf("%d",&a);
    scanf("%d",&b);
    printf("GGT: %d\n",ggT(a,b));
    return 0;
}