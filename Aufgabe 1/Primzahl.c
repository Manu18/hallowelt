#include <stdio.h>
#include <math.h>

int ggT(int a, int b) {
    if(a == 0) {
        return b;
    } else {
        while (b != 0)  {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
}

int istPrim(int p) {
    if (p == 1) {
        return 0;
    }
    for (int i = 2; i < p - 1; i++) {
        if (ggT(p,i) != 1) {
            // keine Prim
            return 0;
        }
    }
    return 1;
}

void primZwilling(int prim) {
    for (int i = 3; i <= 500; i++) {
        if (istPrim(i) == 1) {
            if (abs(prim - i) == 2) {
                printf("Primzahl: %d \nZwilling: %d\n",prim,i);
            }
        }
    }
}


int main() {
    int a;
    int b;
    scanf("%d",&a);
    scanf("%d",&b);
    printf("GGT: %d\n",ggT(a,b));
    printf("Primzahlzwillingsberechnung: \n");
    int p;
    scanf("%d",&p);
    primZwilling(p);
    return 0;
}