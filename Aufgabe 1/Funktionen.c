#include <math.h>
#include <stdio.h>

// A1 a
double summe(double a, double b) {
    return a + b;
}

double differenz(double a, double b) {
    return a - b;
}

double produkt(double a, double b) {
    return a * b;
}

double quatient(double a, double b) {
    return a / b;
}

int runden(double a) {
    return (int) round(a);
}

int main() {
    double a = 12.2;
    double b = 65.12;

    printf("Summe: %f \n", summe(a,b));
    printf("gerundet: %d \n", runden(summe(a,b)));
    printf("Differenz %f \n", differenz(a,b));
    printf("gerundet: %d \n", runden(differenz(a,b)));
    printf("Produkt: %f \n", produkt(a,b));
    printf("gerundet: %d \n", runden(produkt(a,b)));
    printf("Quoatient: %f \n", quatient(a,b));
    printf("gerundet: %d \n", runden(quatient(a,b)));
    return 0;
}