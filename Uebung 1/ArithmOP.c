#include <stdio.h>

int arith(int a, int b, int flag) {
    switch (flag) {
        case 1:
            return a + b;
        case 2:
            return a - b;
        case 3:
            return a * b;
        case 4:
            return a / b;
        case 5:
            return a;
    }
}


int main() {
    int a = 1;
    int b = 2;
    int flag = 4;

    int erg = arith(a,b,flag);
    printf("Ergebnis: %d", erg);

    return 0;
}