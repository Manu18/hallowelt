#include <stdio.h>

int Fib(int f) {
    if (f == 0)
        return 0;
    if (f == 1)
        return 1;
    if (f > 1) {
        return Fib(f - 1) + Fib(f - 2);
    }
}

int Fib2(int input) {
    int fib = 1;
    int fib1 = 1;
    int fib2 = 1;
    int i = 3;
    while (i <= input){
        fib =  fib1 + fib2;
        fib1 = fib2;
        fib2 = fib;
        i++;
    }
    return fib;
}

int main() {
    int input = 0;
    scanf("%d", &input);
    printf("Fib 1: %i",Fib(input));
    printf("\nFib 2: %i",Fib2(input));

    return 0;
}