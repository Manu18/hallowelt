#include <stdio.h>

int min(int a, int b, int c, int d) {
    int min = a;
    if (a > b) {
        min = b;
    }
    if (b > c) {
        min = c;
    }
    if (c > d) {
        min = d;
    }
    return min;
}

int max(int a, int b, int c, int d) {
    int max = a;
    if (a < b) {
        max = b;
    }
    if (b < c) {
        max = c;
    }
    if (c < d) {
        max = d;
    }
    return max;
}

int main() {
    int a = 1;
    int b = 2;
    int c = 3;
    int d = 4;

    int minimum = min(a,b,c,d);
    int maximum = max(a,b,c,d);

    printf("Min %d\n", minimum);
    printf("Max %d\n", maximum);

    // Goto For Schleife
    int i = 0;
    Start:;
    if (i < 5) {
        printf("%d\n", i);
        i++;
        goto Start;
    }


    // Willkommen bei der vorletzten Aufgabe dieser Uebungseinheit.
    goto A;
    C:
    printf("der ");
    goto D;
    B:
    printf("bei ");
    goto C;
    A:
    printf("Willkommen ");
    goto B;
    E:
    printf("Uebungseinheit ");
    goto F;
    D:
    printf("vorletzen ");
    printf("Aufgabe ");
    printf("dieser ");
    goto E;
    F:



    return 0;
}