#include <stdio.h>

// A1
int min(int a, int b, int c, int d) {
    int min = a;
    if (a > b) {
        min = b;
    }
    if (b > c) {
        min = c;
    }
    if (c > d) {
        min = d;
    }
    return min;
}
int max(int a, int b, int c, int d) {
    int max = a;
    if (a < b) {
        max = b;
    }
    if (b < c) {
        max = c;
    }
    if (c < d) {
        max = d;
    }
    return max;
}

// A3
int sizeOfInt() {
    int i = 1;
    int c = 0;
    while (i > c) {
        c = i;
        i++;
    }
    return c;
}

void sizeOfInt2() {
    int size = 0;
    while (-1 <  size) {
        size++;
    }
    printf("The Value of size: %d\n", size);
}

// A4
void geradeZahlen() {
    // Input
    int input = 0;
    scanf("%d", &input);

    for (int i = 0; i < input; i += 2) {
        printf("%d ", i);
    }
}

int main() {

    int x = 4 % 7 /5;
    int y = 3 * x++ + 5 * 10 % 7;

    int a = 6, b = 7, c = 8;
    printf("%d %d\n", 1&2, 1&&2);

    //int minimum = min(a,b,c,d);
    //int maximum = max(a,b,c,d);

    //printf("Min %d\n", minimum);
    //printf("Max %d\n", maximum);

    //printf("Groesster Integer: %d", sizeOfInt());
    sizeOfInt2();
    //geradeZahlen();

    return 0;
}
