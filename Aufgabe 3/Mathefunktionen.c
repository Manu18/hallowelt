#include <math.h>
#include <stdio.h>

int ggT(int a, int b) {
    if(a == 0) {
        return b;
    } else {
        while (b != 0)  {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
}

// A1
void addBruch(int zaehler1, int nenner1, int zaehler2, int nenner2) {
    int zaehler3 = (zaehler1 * nenner2) + (zaehler2 * nenner1);
    int nenner3 = nenner1 * nenner2;
    printf("Ergebnis: %d / %d\n", zaehler3, nenner3);
    int nenner4 = nenner3 / ggT(zaehler3,nenner3);
    int zaehler4 = zaehler3 / ggT(zaehler3,nenner3);
    printf("gekuerztes Ergebnis: %d / %d\n", zaehler4, nenner4);
}

// A2
void integral(int start, int ende, double genau) {
    double integral;
    printf("f(x) = x3 - 2x2 - x + 2: \n");
    double x;
    for (double i = start; i <= ende; i += genau) {
        x = (i*i*i) - 2 * (i*i) - i + 2;
        //printf("f(%lf) = %lf\n",i, x);
        double i2 = i + genau;
        double x2 = (i2*i2*i2) - 2 * (i2*i2) - i2 + 2;
        //printf("f+1(%lf) = %lf\n",i2, x2);
        integral += ((x2 + x) * (i2 - i)) / 2;
    }
    printf("\n\nIntegral von f(x) ist: %lf\n\n", integral);
}

double g(double i) {
    double erg;
    if (i == 0) {
        erg = 1;
        //printf("g(%lf) = %lf\n", i, erg);
    } else {
        erg = sin(i) / i;
        //printf("f(%lf) = %lf\n", i, erg);
    }
}

void integral2(int start, int ende, double genau) {
    printf("g(x) = sin(x) / x \n");
    double x;
    double integral;
    for (double i = start; i <= ende; i += genau) {
        double x2;
        double i2 = i + genau;
        x = g(i);
        x2 = g(i2);
        integral += ((x2 + x) * (i2 - i)) / 2;
    }
    printf("\n\nIntegral von g(x) ist: %lf\n\n", integral);
}

//A3
double hsqrt(double a) {
    double epsilon = 0.0001;
    double fehler;
    double y;
    y = (a + 1) / 2;
    do {
        y = (y + a / y) / 2.0;
        fehler = fabs(a - (y * y));
        printf("y=%lf \n", y);
    } while(fehler > epsilon);
    return y;
}


int main() {
    addBruch(30,15,20,4);
    int start;
    int ende;
    double genau;
    /*
    printf("Start, Ende, Genauigkeit eingeben: \n");
    scanf("%i", &start);
    scanf("%i", &ende);
    scanf("%lf", &genau);
    integral(start,ende,genau);
     */
    printf("Integral 2: \n");
    //integral(-1,2,0.01);
    integral(-1,1234,0.01);

    /*
    printf("Integral 3: \n");
    integral2(-1000,1000,0.1);

    double w = 9;
    printf("Heron-Wurzel: %lf\n",hsqrt(w));
    printf("Wurzel: %lf\n",sqrt(w));

    */

    return 0;
}