#ifndef STACK_H
#define STACK_H

/*
  This depends on a type 'struct data'.
*/

/*
  Data structure describing a single stack frame. This is also the
  node structure for the underlaying linked list structure. If a new
  stack frame is created, i.e. an element is pushed onto the stack,
  *next is set to point to the previous element. Data holds the
  precious data of this stack frame.
*/
struct stack_frame {
    struct stack_frame *next;
    struct data *data;
};


/*
  The 'handle' to the whole stack. top_of_stack always points to the,
  well, top of the stack.
*/
struct stack_t {
    struct stack_frame *top_of_stack;
};


/*
  Construct a new empty stack.

  @param none
  @return The created stack, or NULL on failure
*/
struct stack_t *create(void);

/*
  Discard all elements on the stack.

  @param stack The stack
  @return none
*/
void discard(struct stack_t **stack);

/*
  Push a new element onto the stack.

  @param stack The stack
  @param e The payload we want to push
  @return 0 on success, or 1 on failure
*/
int push(struct stack_t *stack, struct data *e);

/*
  Pop the latest element from the stack.

  @param stack The stack
  @return The payload, or NULL on failure
*/
struct data *pop(struct stack_t *stack);

/* 
   Test if the stack is empty.

   @param stack The stack
   @return 1 if the stack is empty, 0 if not
*/
int is_empty(struct stack_t *stack);

#endif
