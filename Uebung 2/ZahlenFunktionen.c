#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A1
int getPruefsumme(int number) {
    number += 2;
    number *= 17;
    number %= 127;
    return number;
}

// A2
int add(int a, int b) {
    int erg = 0;
    int add = 0;
    int s = 0;
    int c1 = a;
    int c2 = b;

    do {
        add = c1 ^ c2;
        s = (c1 & c2) << 1;
        c1 = add;
        c2 = s;
        erg = add;
    } while (s != 0);
    return erg;
}

int subtract(int a, int b) {
    int zb = ~b;
    zb = add(zb, 1);
    int erg = add(a,zb);
    return erg;
}

int multiply(int a, int b) {
    int erg = 0;
    if (a > 0 && b > 0) {
        for (int i = 0; i < a; i++) {
            erg = add(erg, b);
        }
        return erg;
    }
    if (a < 0 && b > 0) {
        a = -a;
        for (int i = 0; i < a; i++) {
            erg = add(erg, b);
        }
        return -erg;
    }
    if (a > 0 && b < 0) {
        b = -b;
        for (int i = 0; i < a; i++) {
            erg = add(erg, b);
        }
        return -erg;
    }
    if (a < 0 && b < 0) {
        a = -a;
        b = -b;
        for (int i = 0; i < a; i++) {
            erg = add(erg, b);
        }
        return erg;
    }
}

// A3
void getnNotenschnitt() {
    int input = -1;
    double anz = 0;
    double gesamt = 0;
    double schnitt = 0.0;
    scanf("%d", &input);

    while (input != 0) {
        anz++;
        gesamt += input;
        schnitt = gesamt / anz;
        printf("%f \n", schnitt);
        scanf("%d", &input);
    }
}

// A4
unsigned int random(unsigned int start, unsigned int end) {
    int z = rand() % (end - (start - 1));
    z += start;
    return z;
}

// A5
char *substring(char *string, int start, int end) {
    if (end < 0) {
        end = strlen(string);
    }
    int length = 1 + (end - start);
    char *pointer;
    int c;
    pointer = malloc(length + 1);
    if (pointer == NULL) {
        printf("Unable to allocate memory.\n");
        exit(1);
    }
    for (c = 0 ; c < length ; c++) {
        *(pointer + c) = *(string + start);
        string++;
    }
    *(pointer + c) = '\0';
    return pointer;
}

int summe(char *s) {
    int summe = 0;
    int pointerS = 0;
    for (int i = 0; i < strlen(s); i++) {
        if (s[i] == ',') {
            char *zahl = substring(s, pointerS, - 1);
            summe += atoi(zahl);
            pointerS = i + 1;
        }
    }
    char *zahl = substring(s, pointerS,-1);
    summe += atoi(zahl);
    return summe;
}

int main() {
    int input = 8;

    /*
    printf("%d", getPruefsumme(input));
    printf("\n%d", add(2,123));
    printf("\n%d", subtract(10,3));
    printf("\n%d", multiply(10,3));
    printf("\n%d", multiply(-10,3));
    printf("\n%d", multiply(10,-3));
    printf("\n%d", multiply(-10,-3));
    getnNotenschnitt();
    printf("%d \n", random(10,100));
    printf("%d \n", random(10,14));
      */
    // String mit komma getrennten Zahlen eingeben
    char *stringofnumbers;
    gets(stringofnumbers);
    printf("%d", summe(stringofnumbers));
    return 0;
}