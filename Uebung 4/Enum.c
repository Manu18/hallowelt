#include <stdio.h>

enum bool {false = 0, true = 1};
typedef enum bool boolean;

int main() {
    boolean b = true;
    if (b == true) {
        printf("Es scheint zu funktionieren!");
    } else {
        printf("Es funktioniert nicht");
    }
    return 0;
}