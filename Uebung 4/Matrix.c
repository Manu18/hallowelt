#include <stdio.h>
#include <math.h>

union matrixvektor {
    int a[2][2];
    int b[4];
};
typedef union matrixvektor MV;

void inputVektor(union matrixvektor* mv) {
    for (int i = 0; i < 4; i++) {
        printf("Einkabe der Koordinate %d\n", i);
        scanf("%d", &mv->b[i]);
        printf("\n");
    }
}

void inputMatrix(union matrixvektor* mv) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("Einkabe der Koordinate von Zeile: %d, Spalte: %d\n", i,j);
            scanf("%d", &mv->a[i][j]);
            printf("\n");
        }
    }
}

int vektorbetrag(int* v, int length) {
    int betrag;
    for (int i = 0; i < length; i++) {
        betrag += v[i] * v[i];
    }
    betrag = sqrt(betrag);
    return betrag;
}

int determinante2x2(union matrixvektor* mv) {
    int det;
    det = mv->a[0][0] + mv->a[1][1];
    det -= mv->a[0][1] + mv->a[1][0];
    return det;
}

int main() {
    MV vektor;
    inputVektor(&vektor);
    printf("Betrag Vektor: %d \n", vektorbetrag((int *) &vektor, 4));
    inputMatrix(&vektor);
    printf("Betrag Matrix: %d\n", determinante2x2(&vektor));
    return 0;
}