#include <stdio.h>
#include <string.h>

// A3
enum TV_Hersteller {Samsung, Sony, LG, Philips, Panasonic};
typedef enum TV_Hersteller Hersteller;

// A1
struct Fernseher {
    char artikelname[10];
    int id;
    Hersteller hersteller;
    double preis;
};
typedef struct Fernseher TV;

void tvInput(TV *in) {
    printf("Eingabe Fehernseher (Artikelname, ID, Hersteller, Preis): \n");
    printf("Artikelname: \n");
    scanf("%s", &in->artikelname);
    printf("ID: \n");
    scanf("%d", &in->id);
    printf("Hersteller: \n");
    char h[10];
    scanf("%s", &h);
    printf("Preis: \n");
    scanf("%lf", &in->preis);
    printf("\n");

    if (strcmp(h, "Samsung") == 0) {
        in->hersteller = Samsung;
    } else if (strcmp(h, "Sony") == 0) {
        in->hersteller = Sony;
    } else if (strcmp(h, "LG") == 0) {
        in->hersteller = LG;
    } else if (strcmp(h, "Philips") == 0) {
        in->hersteller = Philips;
    } else if (strcmp(h, "Panasonic") == 0) {
        in->hersteller = Panasonic;
    }
}

void tvArrInput(TV *in, int length) {
    for (int i = 0; i < length; i++) {
        tvInput(&in[i]);
    }
}

void tvOutput(TV out) {
    //printf("Fehrnseher: \nArtikelname: %s \nID: %d \nHersteller: %s \nPreis: %f \n \n", out.artikelname, out.id, out.hersteller, out.preis);
    char *h;
    switch(out.hersteller) {
        case Samsung:
            h = "Samsung";
            break;
        case Sony:
            h = "Sony";
            break;
        case LG:
            h = "LG";
            break;
        case Philips:
            h = "Philips";
            break;
        case Panasonic:
            h = "Panasonic";
            break;
    }
    printf("Fehrnseher: \nArtikelname: %s \nID: %d \nHersteller: %s \nPreis: %f \n \n", out.artikelname, out.id, h, out.preis);
}

void tvArrOutput(TV *out, int length) {
    for (int i = 0; i < length; i++) {
        printf("Array: %d \n", i);
        tvOutput(out[i]);
    }
}

int main() {
    TV tv1, tv2, tv3;
    tvInput(&tv1);
    tvOutput(tv1);

    TV arr[3];
    arr[0] = tv1;
    arr[1] = tv2;
    arr[2] = tv3;

    //tvArrInput(arr,3);
    //tvArrOutput(arr,3);
    //tvOutput(tv2);

    return 0;
}