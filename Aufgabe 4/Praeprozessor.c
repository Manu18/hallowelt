#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define e 2.718281828
#define FUNKTION(x) pow(e,x) + 5x + 3
#define ausgabe(variable) printf("Ausgabe: " #variable " = %f\n", variable)

//  Ausgabe: y = 15

// A1
/*
 * #define SUM(a,b) a+b
 * printf("%d", SUM(3,4) * SUM(2,1));
 * => "21"
 */

// A2
int* array(int *arr) {
    for (int i = 1; i <= 10; i++) {
        arr[i-1] = i;
    }
    return arr;
}

// A3 fehlerhaft.......
char* toBinaryString(int a) {
    int d = a;
    int len = a;
    int c = 1;
    while (len > 1) {
         len -= pow(2,c);
         c++;
    }
    c++;
    if (a == 0) {
        return "0";
    }
    if (a == 1) {
        return "1";
    }
    char *bin = malloc(c*sizeof(char));
    int i = c - 2;
    int k = c - 2;
    while (i >= 0) {
        if (d % (int) pow(2,i) == 0 && d != 0) {
            d -= (int) pow(2,i);
            bin[k - i--] = '1';
        } else {
            bin[k - i--] = '0';
        }
    };
    bin[c-1] = '\0';
    return bin;
}

long binToLong(char *text) {
    long erg = 0;
    int length = strlen(text);
    for (int i = length; i >= 0; i--) {
        char c = text[length - i];
        int z = c - '0';
        if (z == 1) {
            erg += pow(2, i - 1);
        }
    }
    return erg;
}

// A4
multiply(int* A, int* B, int* C, int n) {
    int (*a)[n] = A;
    int (*b)[n] = B;
    int (*c)[n] = C;

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            // additon
            int code = 0;
            for (int k = 0; k < n; k++) {
                code += a[k][i] * b[j][k];
            }
            c[i][j] = code;
            printf("%d ",code);
        }
        printf("\n");
    }
    return c;
}



int main() {
    printf("%f\n",e);
    ausgabe(e);

    int arr[10];
    int *a = array(arr);
    for (int i = 0; i < 10; i++) {
        printf("%d\n",a[i]);
    }

    char* binC = toBinaryString(9);
    printf("%s\n", binC);





    return 0;
}