#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

// A1
long binToLong(char *text) {
    long erg = 0;
    int length = strlen(text);
    for (int i = length; i >= 0; i--) {
        char c = text[length - i];
        int z = c - '0';
        if (z == 1) {
            erg += pow(2, i - 1);
        }
    }
    return erg;
}

// A4
char charAt(char *text, int pos) {
    return text[pos];
}

int compareTo(char *text1, char *text2) {
    // int comp = strcmp(text1,text2);
    // Annahme strings sind gleich groß
    for (int i = 0; i < strlen(text2); i++) {
        if (text1[i] != text2[i]) {
            return text1[i] - text2[i];
        }
    }
    return 0;
}

int contains(char *text, char *subtext) {
    char *pointer;
    for (int i = 0; i < strlen(text); i++) {
        pointer = text + i;
        if (compareTo(pointer, subtext) == 0) {
            return 1;
        }
    }
    return 0;
}

int equals(char *text1, char *text2) {
    if (strcmp(text1, text2) == 0) {
        return 1;
    }
    return 0;
}

int endsWith(char *text, char *subtext) {
    int p = strlen(subtext) - 1;
    char *pointer = &text + p;
    for (int i = strlen(text) - 1; i >= 0; i--) {
        if (text[i] != subtext[p] && p >= 0) {
            return 0;
        }
        p--;
    }
    return 1;
}

int startsWith(char *text, char *subtext) {
    for (int i = 0; i < strlen(subtext); i++) {
        if (text[i] != subtext[i]) {
            return 0;
        }
    }
    return 1;
}

int replace(char *zielText, char c, char re) {
    for (int i = 0; i < strlen(zielText); i++) {
        if (zielText[i] == c) {
            zielText[i] = re;
            return 1;
        }
    }
    return 0;
}

int replaceAll(char *text, char c, char re) {
    char *pointer = &text;
    int check = 0;
    for (int i = 0; i < strlen(text); i++) {
        if (text[i] == c) {
            text[i] = re;
            check = 1;
        }
        pointer++;
    }
    return check;
}

char *substring(char *string, int position, int length) {
    char *pointer;
    int c;
    pointer = malloc(length+1);
    if (pointer == NULL) {
        printf("Unable to allocate memory.\n");
        exit(1);
    }
    for (c = 0 ; c < length ; c++) {
        *(pointer+c) = *(string+position-1);
        string++;
    }
    *(pointer+c) = '\0';
    return pointer;
}

int main() {
    char *string = "100101101100";
    long zahl = binToLong(string);
    printf("%ld \n", zahl);

    char *testtext = "Hello world123";
    char *testtext1 = "asdf";
    char *testtext2 = "Hello world";
    char *testtext3 = "Hello world";
    char *testtext4 = "world";
    char testtext5[] = "Hello";

    printf("CharAt %c \n", charAt(testtext, 1));
    printf("CompareTo %d \n", compareTo(testtext3,testtext2));
    printf("Contains %d \n", contains(testtext,testtext2));
    printf("Equals %d \n", equals(testtext3,testtext2));
    printf("EndsWith %d \n", endsWith(testtext3,testtext4));
    printf("StartsWith %d \n", startsWith(testtext3,testtext5));
    printf("Replace %d \n", replace(testtext5,'l','o'));
    printf(testtext5);
    printf("\nReplace2 %d \n", replaceAll(testtext5,'l','o'));
    printf(testtext5);



    return 0;
}