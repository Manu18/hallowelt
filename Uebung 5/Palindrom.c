#include <stdio.h>
#include <string.h>

// A2
char* givePalindrome(char* text) {
    int length = strlen(text);
    char pal[length];
    char erg[length + length];
    for (int i = length - 1; i >= 0; i--) {
        pal[length - (i + 1)] = text[i];
    }
    strcpy(erg, text);
    strcat(erg, pal);
    char *ponter = erg;
    return ponter;
}

// A3
int isPalindrome(char* text) {
    int length = strlen(text);
    for (int i = 0; i < length; i++) {
        if (text[i] != text[length - i - 1]) {
            return 0;
        }
    }
    return 1;
}

int main() {
    char *text = "abc";
    char *pal;
    pal = givePalindrome(text);
    printf(pal);
    printf("%s \n", givePalindrome(text));
    // merkwuerdiges verhalten hier.
    int p = isPalindrome(pal);
    printf("%d", p);

    return 0;
}

