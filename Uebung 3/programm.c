#include <stdio.h>

// A8
int main(int argc, char* argv[]) {
    printf("argc: %d \n", argc);
    printf("argv: ");
    for(int i = 1; i < argc; i++) {
        printf("%s ", argv[i]);
    }
    return 0;
}