#include <stdio.h>
#include <string.h>

// A1
void speicherAdresse() {
    int c = 0;
    scanf("%d", &c);
    // adresse
    printf("%p\n", (void*)&c);
}

// A3
void arrOut(int *array, int length) {
    for (int i = 0; i < length; i++) {
        printf("%d", array[i]);
    }
    printf("\n");
    int *pos;
    pos = array;
    for (int i = 0; i < length; i++) {
        printf("%d", *pos);
        pos++;
    }
}

// A4
int* subArray(int *array, int beginIndex) {
    int *erg;
    //memcpy(erg, &array[beginIndex], 2*sizeof(*array));
    erg = &array[beginIndex];
    return erg;
}

// A5
int* createArray3() {
    static int a[5][5][5];
    int counter = 1;
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            for (int k = 0; k < 5; k++) {
                a[i][j][k] = counter;
                counter++;
            }
        }
    }
    return a;
}

int* createArray32() {
    static int a[5][5][5];
    int counter = 1;
    int *pos = a;
    for (int i = 1; i <= 125; i++) {
        *pos = i;
        pos++;
    }
    return a;
}

void arrOut3D(int *array, int length) {
    int *pos;
    pos = array;
    printf("\n");
    for (int i = 0; i < length; i++) {
        printf(" %d ", *pos);
        pos++;
    }
}

// A7
void arrayCopy(int *src, int srcPos, int *dest, int destPos, int length) {
    //int *p = *src + srcPos;
    //int *d = *dest + destPos;
    int c1 = srcPos;
    int c2 = destPos;
    for (int i = 0; i < length; i++) {
        dest[c2] = src[c1];
        c1++;
        c2++;
    }
}


int main() {
    /*
    // A2
    char *c = "ab";
    short s = (short) c;
    printf("%d \n",s);
    // output: 16384
    // es wird eine Zahl ausgegeben, da chars als Zahl format gespeichert werdden. als ASCI gespeichert
    char *c2 = "a";
    short s2 = (short) c2;
    printf("%d",s2);
    // output: 16392
    speicherAdresse();
    int punkte[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    arrOut(punkte,10);
    int *p2 = subArray(punkte,5);
    arrOut(p2,5);

    int *abc = createArray3();
    int *b = createArray32();
    int c[125];
    for (int i = 1; i <= 125; i++) {
        c[i-1] = i;
    }
    arrOut3D(abc,125);
    arrOut3D(c,125);
    arrOut3D(b,125);
    */
    int punkte[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int punkte2[10] = { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
    arrOut(punkte,10);
    printf("\n 2. Array: \n");
    arrOut(punkte2,10);
    arrayCopy(punkte,0,punkte2,5,5);
    printf("\n ArrayCopy: \n");
    //arrOut(punkte,10);
    printf("\n");
    arrOut(punkte2,10);

    return 0;
}